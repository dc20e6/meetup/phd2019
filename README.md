## Infrastructure for PHDays 2019

### Preparation

Before running `terraform apply` in the `terraform` directory you need to create `terraform/99-variables.tf`:

```
variable "do_token" {
    default = "<your_do_token>"
}
variable "ssh_keys" {
    default = ["<id_ssh_key>", "<id_ssh_key>"]
}
```

For getting `id_ssh_key` read [API Documentation](https://developers.digitalocean.com/documentation/v2/#list-all-keys)

### Initialization

```
cd ansible
ansible-playbook -u root -i '<IP>,' 00-prepare-env.yml`
```

### Available playbooks

- 00-prepare-env.yml
- 01-httpd-fix-x0.yml
- 02-install-nginx.yml
- 03-install-gixy.yml
- 04-fix-nginx.yml
- 05-copy-shells.yml
- 06-fix-php-ini.yml
- 07-fix-nginx-again.yml
- 08-make-nginx-and-php-shut-up.yml