provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_droplet" "dc20e6_phd2019" {
  name     = "dc20e6-phd2019"
  region   = "ams3"
  image    = "centos-7-x64"
  size     = "s-1vcpu-1gb"
  ssh_keys = "${var.ssh_keys}"
  tags     = ["dc20e6", "phd2019"]
}

output "droplet_public_ip" {
  value = "${digitalocean_droplet.dc20e6_phd2019.ipv4_address}"
}
